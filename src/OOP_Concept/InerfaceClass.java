package OOP_Concept;

interface Add {
	int VARIABLE_1 = 10; // static by nature AND ALWAYS INITIALISED

	public void add();

}

interface Substact {
	int VARIABLE_2 = 20;// public static final

	public void substact();

}

public class InerfaceClass implements Add, Substact {
	public static void main(String[] args) {
		Add obj = new InerfaceClass(); // refrence var of interface Add
		obj.add();

		Substact obj1 = new InerfaceClass();
		obj1.substact();

		System.out.println("The value of VARIABLE_1 is:" + VARIABLE_1);// direct access STATIC interface variable
		System.out.println("The value of VARIABLE_2is:" + VARIABLE_2);

	}

	@Override
	public void substact() {
		int a1 = 200, b1 = 100, c1;
		c1 = a1 - b1;
		System.out.println("The substraction of a1 and b1 is:" + c1);
	}

	@Override
	public void add() {

		int a = 100, b = 100, c;
		c = a + b;
		System.out.println("The addition of a and b is:" + c);
	}
}
