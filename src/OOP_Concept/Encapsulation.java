package OOP_Concept;

public class Encapsulation {

	int a;
	int b;
	int c;

	public void add() {
		a = 50;
		b = 30;
		c = a + b;
		System.out.println("The addition of a and b is:" + c);
	}

	public static void main(String[] args) {
		Encapsulation obj = new Encapsulation();
		obj.add();

	}

}
