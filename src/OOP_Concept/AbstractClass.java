package OOP_Concept;

abstract class Demo {
	public abstract void add(); // abstract method

	public static void substract() // concrete method //abstract class can have static method
	{
		int a1 = 200, b1 = 100, c1;
		c1 = a1 - b1;
		System.out.println("The substraction of a1 and b1 is:" + c1);

	}
}

public class AbstractClass extends Demo {

	public static void main(String[] args) {
		Demo obj = new AbstractClass(); // refrence of absract class
		obj.add();

		Demo.substract(); // we call static method using class name

	}

	@Override
	public void add() {
		int a = 100, b = 100, c;
		c = a + b;
		System.out.println("The addition of a and b is:" + c);
	}

}
